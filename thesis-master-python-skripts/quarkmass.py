import math
import matplotlib.pyplot as plt
import numpy as np
plt.rc('text',usetex=True)

x_min = 1
x_max = 10*10**(3)
x = np.arange(x_min, x_max, 1, dtype=object)

x1 = np.arange(x_min, x_max, 1)

fig, a1 = plt.subplots(figsize=[3,3])

G_F =  1.1664*10**(-5)
V_ts = 0.04
V_tb = 1
a_e = 7.5188*10**(-3)
C_9 = 4.2734
C_10 = -4.1661
C_SM = 1.333
v_Phi = 1000
M_W = 80.385
M_B = 5.3668
f_B = 227
Bhat_B = 1.33
hbar = 6.5821*10**(-25)

### R_K

R_Kplus = 0.745 + 2*0.126
R_Kminus = 0.745 - 2*0.110

WCX=(4*G_F*V_tb*V_ts*a_e/(4*math.sqrt(2)*math.pi))

y1=[]
y2=[]

for i in range(len(x)):

	plus = 2*x[i]**2*(math.sqrt(R_Kplus*((C_9)**2+(C_10)**2)-(C_10)**2) - (C_9)**2)*WCX
	minus = 2*x[i]**2*(math.sqrt(R_Kminus*((C_9)**2+(C_10)**2)-(C_10)**2) - (C_9)**2)*WCX
	
	y1.append(minus)
	y2.append(plus)

### DeltaM_s

DeltaMplus = 17.757 + 2*0.027 
DeltaMminus = 17.757 - 2*0.027
DeltaM = 17.757

z1=[]
z2=[]

Bhat_Bplus = 1.33 + 2*0.06
Bhat_Bminus = 1.33 - 2*0.06

f_Bplus = 227 + 2*5
f_Bminus = 227 - 2*5



for i in range(len(x)):

	minus=-math.sqrt( -( DeltaMminus/( (G_F * M_W)**(2) * M_B / (6 * (math.pi)**(2))* (V_tb*V_ts)**(2) * (f_Bplus)**(2) * Bhat_Bplus / hbar * 10**(-12) ) - C_SM )*( 2*(G_F*M_W*V_tb*V_ts)**(2)/(math.pi)**(2) )/(v_Phi**(2)/(x[i]**(4))+1/((4*math.pi*x[i])**(2)) ) )

	plus=+math.sqrt( -( DeltaMplus/( (G_F * M_W)**(2) * M_B / (6 * (math.pi)**(2))* (V_tb*V_ts)**(2) * (f_Bminus)**(2) * Bhat_Bminus / hbar * 10**(-12) ) - C_SM )*( 2*(G_F*M_W*V_tb*V_ts)**(2)/(math.pi)**(2) )/(v_Phi**(2)/(x[i]**(4))+1/((4*math.pi*x[i])**(2)) ) )

	z1.append(minus)
	z2.append(plus)

a1.fill_between(x1, z1, z2, color = 'forestgreen', alpha = 0.5)
a1.fill_between(x1, y1, y2, color = 'darkorange', alpha = 0.5)


a1.set_xlabel("$m_Q \mathrm{\ in \ GeV}")
a1.set_ylabel("$\mathrm{Re} [Y_\mathrm{Qb}Y_\mathrm{Qs}^{\star}]$")

xticks = ["$0$","$2000$","$4000$","$6000$","$8000$","$10000$"]
a1.set_xticklabels( xticks, rotation=45)

a1.grid()

plt.tight_layout()
plt.savefig('quarkmass.pdf')
