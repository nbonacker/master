import sys

import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as plt
plt.rc('text',usetex=True)

from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.serif'] = 'computer modern roman'


#from matplotlib.backends.backend_pgf import FigureCanvasPgf
#mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

colors = sys.argv[3::2]
labels = sys.argv[4::2]

for i in range(len(colors)):
    if ',' in colors[i]:
        *rgb, alpha = [float(c) for c in colors[i].split(',')]
    else:
        rgb, alpha = colors[i], 0.5
    colors[i] = color_alpha(rgb, alpha)

print(colors)
print(labels)

fig = plt.figure(figsize=(5.511, 5))

if sys.argv[2] == 'horizontal':
    kwargs = {
        'mode': 'expand',
        'loc': 'lower left',
        'bbox_to_anchor': mpl.transforms.Bbox.from_bounds(0, 0, 1, 1),
        'ncol': len(colors),
    }
elif sys.argv[2] == 'vertical':
    kwargs = {
    }
else:
    raise Exception('Unknown legend type "{}"'.format(sys.argv[2]))

legend = fig.legend(
    handles=[mpl.patches.Patch(fc=c, ec='none') for c in colors],
    labels=labels,
    borderaxespad=0.,
    frameon=False,
    fontsize = 11,
    **kwargs
)

fig.savefig(sys.argv[1], pad_inches=0, bbox_inches='tight')
