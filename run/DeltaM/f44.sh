#! /bin/bash

name="f44"
output="../../data/DeltaM/"$name".hdf5"

args=(
        --output $output
        --store-prerun

        --global-option model ZPrime
        --global-option scan-mode cartesian

        #--fix "m_Q" 1e3
        --scan "m_Q" 0 12e3 --prior flat

        --fix "m_D" 1e3
        #--scan "m_D" 0 40e3 --prior flat

        #--fix "Re{YY_Q}" 0
        --scan "Re{YY_Q}" -1e-0  1e-0 --prior flat

        --fix "Im{YY_Q}" 0
        #--scan "Im{YY_Q}" -4e-2 4e-2 --prior flat
        
        --fix "Re{YY_D}" 0
        #--scan "Re{YY_D}" -1e0 1e0 --prior flat

        --fix "Im{YY_D}" 0
        #--scan "Im{YY_D}" -1e1 1e-1 --prior flat

	--fix "v_Phi" 12e3
        #--scan "v_Phi" 0 1e3 --prior flat

        --nuisance "decay-constant::B_s" 6 --prior gaussian +0.2226 +0.2276 +0.2326
        --nuisance "bag-parameter::B_s"  6 --prior gaussian +1.27   +1.33   +1.39

	--constraint "B-mixing::Delta_m_s@HFAG-2014"
	--constraint "B-mixing::phi_12_s@HFAG-2014"

        --prerun-update      1000 
        --prerun-min         1000
        --prerun-max      1000000
        --chunks         1000
        --chunk-size        10000
        --chains 4
)

log="../../data/DeltaM/"$name".log"

~/eos/src/clients/eos-scan-mc "${args[@]}" "$@"  2>&1 | tee $log
