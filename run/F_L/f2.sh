#! /bin/bash

name="f2"
output="../../data/F_L/"$name".hdf5"

args=(
        --output $output
        #--store-observables-and-proposals

        --global-option model ZPrime
        --global-option scan-mode cartesian

        --fix "m_Q" 1e3
        #--scan "m_Q" 0 40e3 --prior flat

        --fix "m_D" 1e3
        #--scan "m_D" 0 40e3 --prior flat

        #--fix "Re{YY_Q}" 0.1
        --scan "Re{YY_Q}" -1e-3  1e-3 --prior flat

        --fix "Im{YY_Q}" 0
        #--scan "Im{YY_Q}" -4e0 4e0 --prior flat
        
        #--fix "Re{YY_D}" 0.1
        --scan "Re{YY_D}" -1e-3 1e-3 --prior flat

        --fix "Im{YY_D}" 0
        #--scan "Im{YY_D}" -4e0 4e0 --prior flat

        #--scan "v_Phi" 0 1e3 --prior flat

        #--nuisance "decay-constant::B_s" 0.2176 0.2376 --prior gaussian +0.2226 +0.2276 +0.2326
        #--nuisance "bag-parameter::B_s"  1.21   1.45   --prior gaussian +1.27   +1.33   +1.39

        --constraint "B^0->K^*0mu^+mu^-::F_L[1.10,6.00]@LHCb-2015"


        --scan      "formfactors::xi_perp_uncertainty"      6    --prior gaussian              +0.89     +1      +1.11 \
        --scan      "formfactors::xi_par_uncertainty"       6    --prior gaussian              +0.86     +1      +1.14 \

        --scan      "B->K^*::F^V(0)@KMPW2010"      6                --prior log-gamma             +0.24     +0.36   +0.59 \
        --scan      "B->K^*::F^A0(0)@KMPW2010"     6                --prior log-gamma             +0.22     +0.29   +0.39 \
        --scan      "B->K^*::F^A1(0)@KMPW2010"     6                --prior log-gamma             +0.15     +0.25   +0.41 \
        --scan      "B->K^*::F^A2(0)@KMPW2010"     6                --prior log-gamma             +0.13     +0.23   +0.42 \
        --scan      "B->K^*::b^V_1@KMPW2010"       6                --prior log-gamma             -5.2      -4.8    -4 \
        --scan      "B->K^*::b^A0_1@KMPW2010"      6                --prior log-gamma             -21.2     -18.2   -16.9 \
        --scan      "B->K^*::b^A1_1@KMPW2010"      6                --prior log-gamma             -0.46     +0.34   +1.2 \
        --scan      "B->K^*::b^A2_1@KMPW2010"      6                --prior log-gamma             -2.2      -0.85   +2.03 \

        --scan      "CKM::A"                       6                --prior gaussian              +0.814    +0.827  +0.84 \
        --scan      "CKM::lambda"                  6                --prior gaussian              +0.2247   +0.2253 +0.226 \
        --scan      "CKM::rhobar"                  6                --prior gaussian              +0.111    +0.132  +0.153 \
        --scan      "CKM::etabar"                  6                --prior gaussian              +0.336    +0.35   +0.364 \




        --prerun-update      1000 
        --prerun-min         1000
        --prerun-max       100000
        --chunks          4000
        --chunk-size         1000
        --chains 4
)

log="../../data/F_L/"$name".log"

~/eos/src/clients/eos-scan-mc "${args[@]}" "$@"  2>&1 | tee $log
