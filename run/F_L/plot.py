import pickle
import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import itertools
import os

def work(name, specifier,  labelx, labely):
	
	print("test")
	print("open: " + '../../data/F_L/' + name + '/' +  specifier  + '.pkl' )

	with open("../../data/F_L/" + name + "/" +  specifier  + ".pkl", 'rb') as f:
		hist, bins, ranges = pickle.load(f)

	bins_x = bins[0]
	bins_y = bins[1]

	values_array = hist.tolist()

	values = sum(values_array, [])

	values.sort(reverse = True)

	samples = sum(values)

	sigma_3 = samples * 0.9973
	sigma_2 = samples * 0.9545
	sigma_1 = samples * 0.6827
	s_3 = True
	s_2 = True
	s_1 = True

	counter = 0
	print()
	for i in range(len(values)):
		counter += values[i]
		if s_3 and counter > sigma_3:
		        s_3 = False
		        mark_3 = values[i]
		if s_2 and counter > sigma_2:
			s_2 = False
			mark_2 = values[i]
		if s_1 and counter > sigma_1:
			s_1 = False
			mark_1 = values[i]

	maximum = max(values)

	bounds = [0, mark_3, mark_2, mark_1, maximum]

	colors = ['white', 'green', 'limegreen', 'yellow']

	#labelx = sys.argv[2] 
	#labely = sys.argv[3]

	fig = plt.figure()

	plt.pcolor(bins_x, bins_y, hist, cmap=mpl.colors.ListedColormap(colors), vmax = maximum, vmin = mark_3)

	plt.axis([bins[0].min(), bins[0].max(),bins[1].min(),bins[1].max()])

	plt.xlabel(labelx)
	plt.ylabel(labely)

	fig.savefig('../../plots/F_L/' + name + '/' + labelx + '-' + labely + '.pdf')

number = int(sys.argv[2])
labels = []

name  = sys.argv[1]

#os.mkdir('../../plots/R_K/' + name )

for i in range(number):
	labels.append(sys.argv[3+i])

for pars in itertools.combinations(range(number), 2):
	work(name ,  str(pars) , labels[int(pars[0])] , labels[int(pars[1])] )



