import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import h5py

datafile = '../../data/R_K/' + sys.argv[1] + '.hdf5'

labelx = sys.argv[2]

data = h5py.File(datafile, 'r')['main run']['chain #0']['samples']

samples = []

for i in range(len(data)):

	samples.append(data[i][0])


fig = plt.figure()

plt.hist(samples, bins=100, histtype='stepfilled', color='green', normed=1)

plt.xlabel(labelx)

plt.ylabel("Probability")

fig.savefig('../../plots/R_K/' + sys.argv[1] + '/' + sys.argv[2]  + '.pdf')

