import itertools
import multiprocessing
import pickle
import sys
import os

import h5py
import numpy as np

def work(all_data, pars, chunk_size, func, args, i):
    result = []

    data = h5py.File(all_data, 'r')['main run']['chain #{}'.format(i)]['samples']

    chunks = chunk_size * np.arange(len(data) // chunk_size + 1 + (1 if len(data) % chunk_size != 0 else 0))
    chunks[-1] = len(data)
    for b, e in zip(chunks, chunks[1:]):
        print(i, b, e)
        d = data[b:e].T
        d = np.array([d[par] for par in pars])
        result.append(func(d, *args))
    return result

def map_chunked(all_data, pars, chunk_size, func, args):
    l = len(h5py.File(all_data, 'r')['main run'])

# 1 ???
    with multiprocessing.Pool(1) as p:
        h = p.starmap(work, zip([all_data] * l, [pars] * l, [chunk_size] * l, [func] * l, [args] * l, range(l)))

    return list(itertools.chain(*h))

def work_extrema(d):
    return np.array([np.min(d, axis=-1), np.max(d, axis=-1)]).T

def work_histogram(d, ranges):
    return np.histogramdd(d.T, bins=100, range=ranges)

def hist(all_data, pars, ranges):
    chunk_size = int(1e5)

    if ranges is None:
        ranges = map_chunked(all_data, pars, chunk_size, work_extrema, [])

        ranges = np.hstack(ranges)
        ranges = np.array([np.min(ranges, axis=-1), np.max(ranges, axis=-1)]).T

    hists = map_chunked(all_data, pars, chunk_size, work_histogram, [ranges])

    bins = hists[0][1]
    hists = [h[0] for h in hists]

    hist = np.sum(hists, axis=0)
    hist = hist.T

    print("bins = " + str(bins))
    print("ranges = " + str(ranges))
    print("hist = " + str(hist[0]))

    return hist, bins, ranges

def dohist(all_data,parameters):

	histogram, bins, ranges = hist(all_data, parameters, None)

	with open('../../data/CombinedConstraints/' + sys.argv[1] + '/'  + str(parameters) + '.pkl', 'wb') as f:
		pickle.dump((histogram, bins, ranges), f)


#os.makedirs('../../data/R_K/' + sys.argv[1])

all_data = '../../data/CombinedConstraints/' + sys.argv[1] + '.hdf5'

for pars in itertools.combinations(range(int(sys.argv[2])), 2):
	dohist(all_data,pars)
