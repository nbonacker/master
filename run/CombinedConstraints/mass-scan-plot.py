import pickle
import sys
import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.rc('text',usetex=True)
import itertools
import os
import libeos
import math

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

def work(constraint ,name, specifier, colors, number, plotrange, Scaling,v_Phi, m_QD):
	
	with open("../../data/" + constraint +  "/" + name + "/" +  specifier  + ".pkl", 'rb') as f:
		hist, bins, ranges = pickle.load(f)


	if(Scaling==0):
		bins_x = bins[0]
		bins_y = bins[1]
	if(Scaling==1):
		bins_x = bins[0]*(m_QD**2)
		bins_y = bins[1]*(m_QD**2)
	if(Scaling==2):
		bins_x = bins[0]*math.sqrt( ( 1+1/(16*(math.pi**2))  ) / ( (v_Phi**2)/(m_QD**4)+1/(16*(math.pi**2)*(m_QD**2)) ) )
		bins_y = bins[1]*math.sqrt( ( 1+1/(16*(math.pi**2))  ) / ( (v_Phi**2)/(m_QD**4)+1/(16*(math.pi**2)*(m_QD**2)) ) )


	levels = [2]
	colors = colors[number]

	cmap = mpl.colors.ListedColormap(colors)
	norm = libeos.region_norm(hist, levels)
	plt.pcolor(bins_x, bins_y, hist, cmap=cmap, norm=norm)

	plt.axis(plotrange)

number = int(sys.argv[12])
output = sys.argv[1]

constraint = []
name = []
specifier =[]
color = []
plotrange = [float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5])]
Scaling = []


for i in range(number):
	constraint.append(sys.argv[5*i+13])
	name.append(sys.argv[5*i+14])
	specifier.append(sys.argv[5*i+15])
	color.append(sys.argv[5*i+16])
	Scaling.append(int(sys.argv[5*i+17]))

for i in range(len(color)):
    if ',' in color[i]:
        *rgb, alpha = [float(c) for c in color[i].split(',')]
    else:
        rgb, alpha = color[i], 0.5
    color[i] = color_alpha(rgb, alpha)

colors = [[color_alpha('white', 0), c] for c in color]

points = 448.13
inch = 72.27
width = points/(inch*2)

fig, a1 = plt.subplots(figsize=[width,width])

v_Phi = int(sys.argv[10])
m_QD = int(sys.argv[11])

for i in range(number):
	work(constraint[i], name[i] ,  specifier[i] , colors, i, plotrange, Scaling[i], v_Phi, m_QD)

a1.set_xlabel(sys.argv[6])
a1.set_ylabel(sys.argv[7])
a1.grid()


#yticklabel = ["$-4$","$-2$","$0$","$2$","$4$"]
xticklabel = ["$0$","$10$","$20$","$30$","$40$","$50$","$60$","$70$","$80$"]



#a1.set_xticks(xticks)
a1.set_xticklabels( xticklabel, rotation=45)

#a1.set_yticks(yticks)
#a1.set_yticklabels( yticklabel, rotation=45)


plt.tight_layout()
plt.savefig('../../plots/CombinedConstraints/' + output + '.pdf')
