#! /bin/bash

name="f9-pmc-1-20"
observable="RK*"
input="../..//data/CombinedConstraints/${name}.hdf5"
output="../../data/CombinedConstraints/${name}${observable}.hdf5"

args=(
        --global-option model ZPrime
        --global-option scan-mode cartesian

        --kinematics s_min 1
        --kinematics s_max 6
        --observable "B->K^*ll::R_K^*@LargeRecoil"

        --pmc-input $input 0 7999999
        --pmc-sample-directory "data/final"

        --fix "m_Q" 20e3
        --fix "m_D" 20e3
        --fix "Re{YY_D}" 0
        --fix "Im{YY_D}" 0

        --output $output
        --store-parameters 1

)

log="../../data/CombinedConstraints/"$name".log"

~/eos/src/clients/eos-propagate-uncertainty "${args[@]}" "$@"  2>&1 | tee $log
