#! /bin/bash

name="f1"
output="../../data/CombinedConstraints/"$name".hdf5"

args=(
        --output $output
        #--store-observables-and-proposals

        --global-option model ZPrime
        --global-option scan-mode cartesian

        --fix "m_Q" 1e3
        #--scan "m_Q" 0 40e3 --prior flat

        --fix "m_D" 1e3
        #--scan "m_D" 0 40e3 --prior flat

        #--fix "Re{YY_Q}" 0
        --scan "Re{YY_Q}" -1e-3  1e-3 --prior flat

        --fix "Im{YY_Q}" 0
        #--scan "Im{YY_Q}" -1e0 1e0 --prior flat
        
        #--fix "Re{YY_D}" -0.1
        --scan "Re{YY_D}" -1e-3 1e-3 --prior flat

        --fix "Im{YY_D}" 0
        #--scan "Im{YY_D}" -4e0 4e0 --prior flat

        #--scan "v_Phi" 0 1e3 --prior flat

        #--nuisance "decay-constant::B_s" 0.2176 0.2376 --prior gaussian +0.2226 +0.2276 +0.2326
        #--nuisance "bag-parameter::B_s"  1.21   1.45   --prior gaussian +1.27   +1.33   +1.39

        --constraint "B^+->K^+l^+l^-::R_K[1.00,6.00]@LHCb-2014"
	--constraint "B^0->K^*0mu^+mu^-::A_FB[1.10,6.00]@LHCb-2015"
	--constraint "B^0->K^*0mu^+mu^-::F_L[1.10,6.00]@LHCb-2015"
	--constraint "B^0->K^*0mu^+mu^-::BR[1.00,6.00]@LHCb-2013"
	--constraint "B^0->K^*0mu^+mu^-::P'_5[1.10,6.00]@LHCb-2015"


        --prerun-update      1000 
        --prerun-min         1000
        --prerun-max       100000
        --chunks           400
        --chunk-size         1000
        --chains 4
)

log="../../data/CombinedConstraints/"$name".log"

~/eos/src/clients/eos-scan-mc "${args[@]}" "$@"  2>&1 | tee $log
