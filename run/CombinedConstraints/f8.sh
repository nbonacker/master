#! /bin/bash

name="f8"
output="../../data/CombinedConstraints/"$name".hdf5"

args=(
        --output $output
        #--store-observables-and-proposals

        --global-option model ZPrime
        --global-option scan-mode cartesian

        #--fix "m_Q" 2e3
        --scan "m_Q" 0 8e3 --prior flat

        --fix "m_D" 1e3
        #--scan "m_D" 0 40e3 --prior flat

        --fix "Re{YY_Q}" 0.01
        #--scan "Re{YY_Q}" -2e-2  2e-2 --prior flat

        --fix "Im{YY_Q}" 0
        #--scan "Im{YY_Q}" -2e0 2e0 --prior flat
        
        --fix "Re{YY_D}" 0
        #--scan "Re{YY_D}" -2e-2 25e-3 --prior flat

        --fix "Im{YY_D}" 0
        #--scan "Im{YY_D}" -4e0 4e0 --prior flat

        #--scan "v_Phi" 0 1e3 --prior flat
        --fix "v_Phi" 1e3 

        --nuisance "decay-constant::B_s" 6 --prior gaussian +0.2226 +0.2276 +0.2326
        --nuisance "bag-parameter::B_s"  6 --prior gaussian +1.27   +1.33   +1.39

        --constraint "B^+->K^+l^+l^-::R_K[1.00,6.00]@LHCb-2014"
	--constraint "B^0->K^*0mu^+mu^-::P'_5[1.10,6.00]@LHCb-2015"
	--constraint "B-mixing::Delta_m_s@HFAG-2014"
	--constraint "B-mixing::phi_12_s@HFAG-2014" #add constraint phi_12 


        --prerun-update      1000 
        --prerun-min         1000
        --prerun-max       100000
        --chunks          2000
        --chunk-size         1000
        --chains 4
)

log="../../data/CombinedConstraints/"$name".log"

~/eos/src/clients/eos-scan-mc "${args[@]}" "$@"  2>&1 | tee $log
