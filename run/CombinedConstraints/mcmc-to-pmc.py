import sys

import h5py
import numpy as np

chunk_size = int(1e6)

def convert_mcmc(output_file_name, input_file_name):
    output_file = h5py.File(output_file_name, 'w')
    input_file = h5py.File(input_file_name, 'r')
    samples = input_file['main run']['chain #0']['samples']
    input_file.copy('descriptions/main run/chain #0', output_file, name='/descriptions')
    nchains = len(input_file['main run'])

    output_samples = output_file.create_dataset(
            '/data/final/samples',
            shape=(len(samples) * nchains,),
            dtype=np.dtype((samples.dtype.base, (samples.dtype.shape[0] + 2,)))
            )

    for i, chain in enumerate(input_file['main run']):
        chain = input_file['main run'][chain]['samples']

        chunks = chunk_size * np.arange(len(samples) // chunk_size + 1 + (1 if len(samples) % chunk_size != 0 else 0))
        chunks[-1] = len(samples)

        for b, e in zip(chunks, chunks[1:]):
            print(i, b, e)
            d = chain[b:e]
            output_samples[b + len(samples) * i : e + len(samples) * i] = np.hstack((d[:,:-1], np.zeros((e - b, 2)), d[:,-1:]))

    input_file.close()
    output_file.close()

input_file = '../../data/CombinedConstraints/' + sys.argv[1] + '.hdf5'
output_file = '../../data/CombinedConstraints/' + sys.argv[1] + 'pmc.hdf5'

convert_mcmc(output_file, input_file) 
