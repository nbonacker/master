import pickle
import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.rc('text',usetex=True)
import itertools
import os
import libeos
import math

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

def work(constraint ,name, specifier, colors, number, plotrange, Scaling,v_Phi, m_QD):
	
	with open("../../data/" + constraint +  "/" + name + "/" +  specifier  + ".pkl", 'rb') as f:
		hist, bins, ranges = pickle.load(f)


	if(Scaling==0):
		bins_x = bins[0]
		bins_y = bins[1]
	if(Scaling==1):
		bins_x = bins[0]*(m_QD**2)
		bins_y = bins[1]*(m_QD**2)
	if(Scaling==2):
		bins_x = bins[0]*math.sqrt( ( 1+1/(16*(math.pi**2))  ) / ( (v_Phi**2)/(m_QD**4)+1/(16*(math.pi**2)*(m_QD**2)) ) )
		bins_y = bins[1]*math.sqrt( ( 1+1/(16*(math.pi**2))  ) / ( (v_Phi**2)/(m_QD**4)+1/(16*(math.pi**2)*(m_QD**2)) ) )


	levels = [2]
	colors = colors[number]

	cmap = mpl.colors.ListedColormap(colors)
	norm = libeos.region_norm(hist, levels)
	plt.pcolor(bins_x, bins_y, hist, cmap=cmap, norm=norm)

	plt.axis(plotrange)

number = int(sys.argv[12])
output = sys.argv[1]

constraint = []
name = []
specifier =[]
color = []
plotrange = [float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5])]
Scaling = []


for i in range(number):
	constraint.append(sys.argv[5*i+13])
	name.append(sys.argv[5*i+14])
	specifier.append(sys.argv[5*i+15])
	color.append(sys.argv[5*i+16])
	Scaling.append(int(sys.argv[5*i+17]))

colors = [[color_alpha('white', 0), color_alpha(c, 0.5)] for c in color]

points = 448.13
inch = 72.27
width = points/(inch*2)

fig, a1 = plt.subplots(figsize=[width,width])

v_Phi = int(sys.argv[10])
m_QD = int(sys.argv[11])

for i in range(number):
	work(constraint[i], name[i] ,  specifier[i] , colors, i, plotrange, Scaling[i], v_Phi, m_QD)

a1.set_xlabel(sys.argv[6])
a1.set_ylabel(sys.argv[7])
a1.grid()

ticksstyle = sys.argv[9]

if(ticksstyle == '9a'):
	xticks = [-0.010,-0.005,0,0.005,0.010]
	cl = ["$-0,010$","$-0,005$","$0,000$","$0,005$","$0,010$"]
	yticks = [-0.010,-0.005,0,0.005,0.010]
	wl = ["$-5$","$-2,5$", "$0$", "$2,5$", "$5$"]

if(ticksstyle == '9b'):
	xticks = [-0.02,-0.01,0,0.01,0.02]
	cl = ["$-0,02$","$-0,01$","$0,00$","$0,01$","$0,02$"]
	yticks = [-0.02,-0.01,0,0.01,0.02]
	wl = ["$-4$","$-2$", "$0$", "$2$", "$4$"]

if(ticksstyle == '9c'):
	xticks = [-0.04,-0.02,0,0.02,0.04]
	cl = ["$-0,04$","$-0,02$","$0,00$","$0,02$","$0,04$"]
	yticks = [-0.04,-0.02,0,0.02,0.04]
	wl = ["$-2$","$-1$", "$0$", "$1$", "$2$"]

if(ticksstyle == '9d'):
	xticks = [-0.08,-0.04,0,0.04,0.08]
	cl = ["$-0,08$","$-0,04$","$0,00$","$0,04$","$0,08$"]
	yticks = [-0.08,-0.04,0,0.04,0.08]
	wl = ["$-1.0$","$-0.5$", "$0.0$", "$0.5$", "$1.0$"]





if(ticksstyle == 'Ra'):
	xticks = [-0.02,-0.01,0,0.01,0.02]
	cl = ["$-0,02$","$-0,01$","$0,00$","$0,01$","$0,02$"]
	yticks = [-0.02,-0.01,0,0.01,0.02]
	wl = ["$-10$","$-5$", "$0$", "$5$", "$10$"]

if(ticksstyle == 'Rb'):
	xticks = [-0.04,-0.02,0,0.02,0.04]
	cl = ["$-0,04$","$-0,02$","$0,00$","$0,02$","$0,04$"]
	yticks = [-0.04,-0.02,0,0.02,0.04]
	wl = ["$-4$","$-2$", "$0$", "$2$", "$4$"]

if(ticksstyle == 'Rc'):
	xticks = [-0.08,-0.04,0,0.04,0.08]
	cl = ["$-0,08$","$-0,04$","$0,00$","$0,04$","$0,08$"]
	yticks = [-0.08,-0.04,0,0.04,0.08]
	wl = ["$-2$","$-1$", "$0$", "$1$", "$2$"]

if(ticksstyle == 'Rd'):
	xticks = [-0.16,-0.08,0,0.08,0.16]
	cl = ["$-0,16$","$-0,08$","$0,00$","$0,08$","$0,16$"]
	yticks = [-0.16,-0.08,0,0.08,0.16]
	wl = ["$-1.0$","$-0.5$", "$0.0$", "$0.5$", "$1.0$"]




a1.set_aspect('equal', 'box')
#a1.set_aspect('equal')
a1.set_xticks(xticks)
a1.set_xticklabels( cl, rotation=45)
a1.set_yticklabels(cl)
a1.set_yticks(yticks)
#a1.ticklabel_format(axis='both', style='sci', scilimits=(0,0))
#a1.ticklabel_format(scilimits=(-2, 4))
a1.plot(0,0,'kx')

#a1.get_yaxis().get_offset_text()

c = 1000/1.7/(m_QD**2)

xlimits = a1.get_xlim()
ylimits = a1.get_ylim()

xwilson = [xlimits[0]*c,xlimits[1]*c]
ywilson = [ylimits[0]*c,ylimits[1]*c]
wilsonstyle = int(sys.argv[8])


if(wilsonstyle == 0):
	ax2 = a1.twinx()
	ax2.set_ylim(ywilson)
	ax2.set_ylabel(r"$\mathrm{Im}\left[\mathcal{C}_{9}\right]$")
	#ax2.grid(color = 'darkred')
	ay2 = a1.twiny()
	ay2.set_xlim(xwilson)
	#ay2.set_xticklabels(wl, rotation=45)
	ay2.set_xlabel(r"$\mathrm{Re}\left[\mathcal{C}_{9}\right]$")
	#ay2.grid(color = 'darkred')

if(wilsonstyle == 1):
	ax2 = a1.twinx()
	ax2.set_ylim(ywilson)
	ax2.set_ylabel(r"$-\mathrm{Re}\left[\mathcal{C}^{\prime}_{9}\right]$")
	#ax2.grid(color = 'darkred')
	ay2 = a1.twiny()
	ay2.set_xlim(xwilson)
	#ay2.set_xticklabels(wl, rotation=45)
	ay2.set_xlabel(r"$\mathrm{Re}\left[\mathcal{C}_{9}\right]$")
	#ay2.grid(color = 'darkred')

plt.tight_layout()
plt.savefig('../../plots/CombinedConstraints/' + output + '-m-QD-' + str(m_QD) + '-v-Phi-' + str(v_Phi)  + '.pdf')
