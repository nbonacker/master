import pickle
import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.rc('text',usetex=True)
import itertools
import os
import libeos

def work(directory, name, specifier,  labelx, labely):
	
	print("open: " + '../data/' + directory + '/' + name + '/' +  specifier  + '.pkl' )

	with open("../data/" + directory + "/" + name + "/" +  specifier  + ".pkl", 'rb') as f:
		hist, bins, ranges = pickle.load(f)

	bins_x = bins[0]
	bins_y = bins[1]

	levels = [1, 2, 3]
	colors = ['w'] + ['yellow'] + ['orange'] + ['red']

	plotrange = [bins[0].min(), bins[0].max(),bins[1].min(),bins[1].max()]

	cmap = mpl.colors.ListedColormap(colors)
	norm = libeos.region_norm(hist, levels)

	#labelx = sys.argv[2] 
	#labely = sys.argv[3]

	points = 448.13
	inch = 72.27
	width = points/(inch*2)

	#fig, a1 = plt.subplots(figsize=[width,width])


	fig = plt.figure(figsize=[width,width])
	plt.pcolor(bins_x, bins_y, hist, cmap=cmap, norm=norm, alpha = 0.5)
	plt.axis([bins[0].min(), bins[0].max(),bins[1].min(),bins[1].max()])
	#plt.gca().set_aspect('equal')

	plt.xlabel(labelx)
	plt.ylabel(labely)
	plt.setp(plt.xticks()[1], rotation=45)
	plt.grid()

	plt.axis(plotrange)
	plt.tight_layout()

	fig.savefig('../plots/' + directory + '/' + name + '/' + name + '.pdf')

number = int(sys.argv[3])
labels = []

name  = sys.argv[2]
directory = sys.argv[1]

#os.mkdir('../../plots/R_K/' + name )

for i in range(number):
	labels.append(sys.argv[4+i])

for pars in itertools.combinations(range(number), 2):
	work(directory, name,  str(pars) , labels[int(pars[0])] , labels[int(pars[1])] )
