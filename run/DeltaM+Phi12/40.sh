#! /bin/bash


name="40"

output="${HOME}/master/data/DeltaM+Phi12/${name}.hdf5"
log="${HOME}/master/data/DeltaM+Phi12/${name}.log"

args=(
	--output $output
	#--store-observables-and-proposals
	--global-option model ZPrime
	--global-option scan-mode cartesian
	
	#--fix "m_Q" 1e3
	--scan "m_Q" 0 40e3 --prior flat
	#--fix "m_D" 1e3
	--scan "m_D" 0 40e3 --prior flat

	--fix "Re{YY_Q}" 1
	#--scan "Re{YY_Q}" -4e0  4e0 --prior flat
	--fix "Im{YY_Q}" 0
	#--scan "Im{YY_Q}" -4e-1 4e-1 --prior flat
	--fix "Re{YY_D}" 1
	#--scan "Re{YY_D}" -4e0 4e0 --prior flat
	--fix "Im{YY_D}" 0
	#--scan "Im{YY_D}" -4e-1 4e-1 --prior flat

	--fix "v_Phi" 1e3
	#--scan "v_Phi" 0 1e4  --prior flat

	--nuisance "decay-constant::B_s" 0.2176 0.2376 --prior gaussian +0.2226 +0.2276 +0.2326
	--nuisance "bag-parameter::B_s"  1.21   1.45   --prior gaussian +1.27   +1.33   +1.39
	
	--constraint "B-mixing::Delta_m_s@HFAG-2014"
	--constraint "B-mixing::phi_12_s@HFAG-2014"

	--chunk-size      1000
	--chunks       2000
	--prerun-update  10000
	--prerun-min     20000
	--prerun-max   2000000
	--chains 4
)


~/eos/src/clients/eos-scan-mc "${args[@]}" "$@"  2>&1 | tee $log

